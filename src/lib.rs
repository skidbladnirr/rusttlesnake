pub mod config;
use config::Config;

pub fn run(config: Config) {
    println!("Running... {}x{}", config.width, config.height);
}
