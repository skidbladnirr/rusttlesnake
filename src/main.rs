use std::env;
use std::process;

use rusttlesnake::config::Config;

use rusttlesnake::run;

fn main() {
    let args: Vec<String> = env::args().collect();

    let config = Config::build(&args).unwrap_or_else(|err| {
        eprintln!("\
Error parsing arguments: {err}
  Usage: rusttlesnake [<height> <width>]");
        process::exit(1);
    });

    run(config);
}

