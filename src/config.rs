const MIN_WIDTH: u8 = 5;
const MIN_HEIGHT: u8 = 5;
const MAX_WIDTH: u8 = 20;
const MAX_HEIGHT: u8 = 20;

pub struct Config {
    pub width: u8,
    pub height: u8,
}

impl Config {
    pub fn build(args: &[String]) -> Result<Config,String> {
        let n = args.len();
        if n == 2 || n > 3 {
            println!("{n}");
            return Err(String::from("Incorrect number of arguments."));
        }

        if n == 1 {
            return Ok(Config {
                    width: 10,
                    height: 10,
                })
        }

        let width: u8 = args[1].parse().unwrap();
        let height: u8 = args[2].parse().unwrap();


        if width < MIN_WIDTH || width > MAX_WIDTH {
            return Err(format!(
                        "Incorrect width. The width must be between {} and {}.",
                        MIN_WIDTH,
                        MAX_WIDTH));
        }

        if height < MIN_HEIGHT || height > MAX_HEIGHT {
            return Err(format!("Incorrect height. The height must be between {} and {}.",
                        MIN_HEIGHT,
                        MAX_HEIGHT));
        }

        Ok(Config {
            width,
            height,
        })

    }

}
